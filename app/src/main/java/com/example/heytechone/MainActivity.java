package com.example.heytechone;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.heytechone.fragment.FragmentPhone;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openFirstFragment();
    }
    private void openFirstFragment() {
        FragmentPhone fragment = new FragmentPhone();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameContainer , fragment)
                .addToBackStack(null)
                .commit();
    }
}