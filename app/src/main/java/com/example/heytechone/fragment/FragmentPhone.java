package com.example.heytechone.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.heytechone.R;
import com.example.heytechone.databinding.FragmentPhoneBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class FragmentPhone extends Fragment {
    FragmentPhoneBinding binding;
    EditText phoneET;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       binding = FragmentPhoneBinding.inflate(inflater , container , false);
       return  binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = binding.editTextPhone.getText().toString().trim();
                if(phone.isEmpty() && phone.length() < 10)
                {
                    binding.editTextPhone.setError("enter number");
                }
                else {
                    String phoneNumber = "+" + 91 + phone;
                    FragmentVerifyPhone fragment = new FragmentVerifyPhone();
                    Bundle bundle = new Bundle();
                    bundle.putString("phone" , phoneNumber);
                    fragment.setArguments(bundle);

                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frameContainer , fragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });

    }
}
