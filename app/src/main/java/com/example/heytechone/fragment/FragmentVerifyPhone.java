package com.example.heytechone.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.heytechone.ProfileActivity;
import com.example.heytechone.R;
import com.example.heytechone.databinding.FragmentVerifyPhoneBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class FragmentVerifyPhone extends Fragment implements verifyPhonePresenterContract.View{
    FragmentVerifyPhoneBinding binding;
    String verificationId;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private verifyPhonePresenterContract.presenter presenter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentVerifyPhoneBinding.inflate(inflater, container, false);
        mAuth = FirebaseAuth.getInstance();
        progressBar = binding.progressbar.findViewById(R.id.progressbar);
        presenter = new VerifyPhonePresenter(this);

        Bundle bundle =new Bundle();
        bundle = getArguments();
        if(bundle != null)
        {
            String phoneNumber = bundle.getString("phone");
            presenter.sendVerificationCode(phoneNumber , mCallBack);
        }

        return binding.getRoot();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = binding.editTextCode.getText().toString().trim();
                Log.d("code" , code);

                if (code.isEmpty() || code.length() < 6) {
                    binding.editTextCode.setError("Valid number is required");
                    binding.editTextCode.requestFocus();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                presenter.verifyCode(code);
            }
        });

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks(){
        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                binding.editTextCode.setText(code);
                presenter.verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };


    @Override
    public void onSuccess(String message) {
        Toast.makeText(getContext(), message , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(getContext(), message , Toast.LENGTH_SHORT).show();
    }
}
