package com.example.heytechone.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.heytechone.ProfileActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;

public class VerifyPhonePresenter implements verifyPhonePresenterContract.presenter {
    String verificationId;
    FirebaseAuth mAuth;
    ProgressBar progressBar;
    private FragmentVerifyPhone verifyPhone;

    verifyPhonePresenterContract.View view;
    public VerifyPhonePresenter(FragmentVerifyPhone view){
        this.verifyPhone = view;
        this.view = (verifyPhonePresenterContract.View) view;

    }

    @Override
    public void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }

    @Override
    public void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(verifyPhone.getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    //replace fragment
                    view.onSuccess("success");
                    Intent intent = new Intent(verifyPhone.getContext(), ProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    verifyPhone.startActivity(intent);

                } else {
                    //verification unsuccessful.. display an error message
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        view.onError("something went wrong");
                    }

                }
            }
        });

    }

    @Override
    public void sendVerificationCode(String phoneNumber , PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBack) {
        progressBar.setVisibility(View.VISIBLE);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                verifyPhone.getActivity(),
                mCallBack
        );


    }
}
