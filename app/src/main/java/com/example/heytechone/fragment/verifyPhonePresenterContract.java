package com.example.heytechone.fragment;

import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

public interface verifyPhonePresenterContract {

    interface View{
        void onSuccess(String message);
        void onError(String message);
    }
    interface presenter{
        void verifyCode(String code);
        void signInWithCredential(PhoneAuthCredential credential);
        void sendVerificationCode(String phoneNumber , PhoneAuthProvider.OnVerificationStateChangedCallbacks callback);
    }
}
